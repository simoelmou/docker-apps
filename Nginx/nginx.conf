user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    #include       /etc/nginx/mime.types;
    #default_type  application/octet-stream;
    error_log /etc/nginx/error_log.log warn;
    proxy_cache_path /etc/nginx/cache keys_zone=one:10m max_size=500m;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $request_time $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    include /etc/nginx/conf.d/default.conf;

    # Grafana server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name grafana.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:3000;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    # SonarQube server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name sonarqube.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:9000;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    # Plex server
    upstream plex {
        server 127.0.0.1:32400 max_fails=5 fail_timeout=1m;
    }

    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name plex.simoelmou.ddns.me;

        location / {
            proxy_pass http://plex;
            proxy_read_timeout 10m;
            proxy_connect_timeout 60s;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    # Deluge Torrent server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name deluge.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:8112;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    # Rancher server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name rancher.simoelmou.ddns.me;

        location / {
            proxy_pass https://localhost:8443;
            include /etc/nginx/conf.d/headers.conf;
	    proxy_set_header Upgrade $http_upgrade;
	    proxy_set_header Connection "upgrade";

        }
    }

    # Cockpit server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name cockpit.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:9090;
            #include /etc/nginx/conf.d/headers.conf;
        }
    }

    # Portainer server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name portainer.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:9999;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    # Weave server
    server {
        listen 443 ssl;
        include /etc/nginx/conf.d/ssl.conf;
        proxy_cache one;
        server_name weave.simoelmou.ddns.me;

        location / {
            proxy_pass http://localhost:4040;
            include /etc/nginx/conf.d/headers.conf;
        }
    }

    #sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
}
